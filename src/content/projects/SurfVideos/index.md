---
date: '2018-03-01'
title: 'Academy Youtube'
image: ''
github: 'https://github.com/jonas-kgomo/academy-youtube'
external: ''
tech:
  - React
  - Redux
  - YouTube API
show: 'true'
---

Small React project to help developers find playlists of programming language videos via the YouTube API.

