---
date: '2017-12-01'
title: 'Oarystis VR'
image: ''
github: ''
external: 'https://github.com/jonas-kgomo/OarystisVR'
tech:
  - VR
  - STEAM VR
  - C#
show: 'true'
---

Building VR application for coral reef awareness by exploring Underwater Archaeology .