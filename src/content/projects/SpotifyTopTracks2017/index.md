---
date: '2018-04-20'
title: 'Simulacra VR'
image: ''
github: 'https://github.com/jonas-kgomo/SIMULACRA-VR'
external: 'https://jonas-kgomo.github.io/prosthetic-studios/'
tech:
  - C#
  - Pure Data
show: 'true'
---

Developing virtual reality music instrument app that lets you create and play music
