---
date: '2018-05-01'
title: 'Demo Scene: Audiovisual'
image: ''
github: ''
external: 'https://github.com/jonas-kgomo/Running-Guy'
tech:
  - JavaScript
  - Java
  - Mathieu Henri
  - SCSS
  - JS
show: 'true'
---

Creative Coding : 
Running JavaScript demo scene in Android WebView.

[Live](http://www.p01.org/jsconf_asia_2015/live.htm) | Credits: p01

