---
date: '2014-03-23'
title: 'Simulacra VR'
cover: './halcyon.png'
path: '/simulacra'
github: 'https://github.com/bchiang7/halcyon-site'
external: 'https://halcyon-theme.netlify.com/'
tech:
  - VS Code
  - Sublime Text
  - Atom
  - iTerm2
  - Hyper
show: 'true'
disc: 'My main motivation for building this was so I          
       could cycle through pins on my boards with the
       be at tou ash sd. sdjnkbj sfubidv saldfibf sdhjf . sfubairufbr
       sdkjfbsdiufbi'
---



Simulacra is a tool that helps you design virtual sound worlds with abstract instruments.The infrastructure enable users without musicality develop sophisticated instruments. Simulacra is a hyperinstrument that approaches musical instruments as haptic devices.

Simulacra uses machine learning techniques to create experimental instruments which gives musicians the ability to make music using completely new sounds. It helps experience a cross between music and perception.

[Northeastern University](https://www.ccis.northeastern.edu/)
A minimal, dark blue theme for VS Code, Sublime Text, Atom, iTerm, and more. Available on [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=brittanychiang.halcyon-vscode), [Package Control](https://packagecontrol.io/packages/Halcyon%20Theme), [Atom Package Manager](https://atom.io/themes/halcyon-syntax), and [npm](https://www.npmjs.com/package/hyper-halcyon-theme).

The simulacrum is never that which conceals the truth--it is the truth which conceals that there is none.




$F_x$
The simulacrum is true.

`Sectionalise < f --> `

Ecclesiastes

 
<iframe width="701" height="480" src="https://www.youtube.com/embed/YCe1gC5VaW4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
If we were able to take as the finest allegory of simulation the Borges tale where the cartographers of the Empire draw up a map so detailed that it ends up exactly covering the territory (but where, with the decline of the Empire this map becomes frayed and finally ruined, a few shreds still discernible in the deserts - the metaphysical beauty of this ruined abstraction, bearing witness to an imperial pride and rotting like a carcass, returning to the substance of the soil, rather as an aging double ends up being confused with the real thing), this fable would then have come full circle for us, and now has nothing but the discrete charm of second-order simulacra.l

 

Abstraction today is no longer that of the map, the double, the mirror or the concept. Simulation is no longer that of a territory, a referential being or a substance. It is the generation by models of a real without origin or reality: a hyperreal. The territory no longer precedes the map, nor survives it. Henceforth, it is the map that precedes the territory - precession of simulacra - it is the map that engenders the territory and if we were to revive the fable today, it would be the territory whose shreds are slowly rotting across the map. It is the real, and not the map, whose vestiges subsist here and there, in the deserts which are no longer those of the Empire, but our own. The desert of the real itself.

 

In fact, even inverted, the fable is useless. Perhaps only the allegory of the Empire remains. For it is with the same imperialism that present-day simulators try to make the real, all the real, coincide with their simulation models. But it is no longer a question of either maps or territory. Something has disappeared: the sovereign difference between them that was the abstraction's charm. For it is the difference which forms the poetry of the map and the charm of the territory, the magic of the concept and the charm of the real. This representational imaginary, which both culminates in and is engulfed by the cartographer's mad project of an ideal coextensivity between the map and the territory, disappears with simulation, whose operation is nuclear and genetic, and no longer specular and discursive. With it goes all of metaphysics. No more mirror of being and appearances, of the real and its concept; no more imaginary coextensivity: rather, genetic miniaturization is the dimension of simulation. The real is produced from miniaturized units, from matrices, memory banks and command models - and with these it can be reproduced an indefinite number of times. It no longer has to be rational, since it is no longer measured against some ideal or negative instance. It is nothing more than operational. In fact, since it is no longer enveloped by an imaginary, it is no longer real at all. It is a hyperreal: the product of an irradiating synthesis of combinatory models in a hyperspace without atmosphere.

 

In this passage to a space whose curvature is no longer that of the real, nor of truth, the age of simulation thus begins with a liquidation of all referentials - worse: by their art)ficial resurrection in systems of signs, which are a more ductile material than meaning, in that they lend themselves to all systems of equivalence, all binary oppositions and all combinatory algebra. It is no longer a question of imitation, nor of reduplication, nor even of parody. It is rather a question of substituting signs of the real for the real itself; that is, an operation to deter every real process by its operational double, a metastable, programmatic, perfect descriptive machine which provides all the signs of the real and short-circuits all its vicissitudes. Never again will the real have to be produced: this is the vital function of the model in a system of death, or rather of anticipated resurrection which no longer leaves any chance even in the event of death. A hyperreal henceforth sheltered from the imaginary, and from any distinction between the real and the imaginary, leaving room only for the orbital recurrence of models and the simulated generation of difference.