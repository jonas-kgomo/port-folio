---
date: '2018-12-23'
title: 'Caustic Shareables'
path: '/pinsimple'
cover: './pin-simple.png'
github: 'https://github.com/bchiang7/pin-simple'
external: 'https://pin-simple.herokuapp.com/'
tech:
  - Vue
  - Vue Router
  - Express
  - Pinterest API
show: 'true'
disc: 'A web app for visualizing personalized Spotify         data. View your top artists, top tracks,               recently played tracks, and detailed audio             information about each track. Create and save          new playlists of recommended tracks based on           your existing playlists and more.'
---

Pinterest, but just the pictures.

My main motivation for building this was so I could cycle through pins on my boards with the left and right arrow keys and hide the other distracting features that I don't usually use on Pinterest.
