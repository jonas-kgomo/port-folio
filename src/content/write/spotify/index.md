---
date: '2019-12-23'
title: 'Spotify Profile'
path: '/spotify'
cover: './demo.png'
github: 'https://github.com/bchiang7/spotify-profile'
external: 'https://spotify-profile.herokuapp.com/'
tech:
  - Reacts
  - Express
  - Reach Router
  - Styled Components
show: 'true'
disc: 'A web app for visualizing personalized Spotify         data. View your top artists, top tracks,               recently played tracks, and detailed audio             information about each track. Create and save          new playlists of recommended tracks based on           your existing playlists and more.'
---

A web app for visualizing personalized Spotify data. View your top artists, top tracks, recently played tracks, and detailed audio information about each track. Create and save new playlists of recommended tracks based on your existing playlists and more.
