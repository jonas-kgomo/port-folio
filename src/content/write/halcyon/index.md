---
date: '2014-03-23'
title: 'Biological Informatics'
cover: './halcyon.png'
path: '/halcyon'
github: 'https://github.com/bchiang7/halcyon-site'
external: 'https://halcyon-theme.netlify.com/'
tech:
  - VS Code
  - Sublime Text
  - Atom
  - iTerm2
  - Hyper
show: 'true'
disc: 'A web app for visualizing personalized Spotify         data. View your top artists, top tracks,               recently played tracks, and detailed audio  [Northeastern University](https://www.ccis.northeastern.edu/)           information about each track. Create and save          new playlists of recommended tracks based on           your existing playlists and more.'
---

What we learned from micro-organisms


A minimal, dark blue theme for VS Code, Sublime Text, Atom, iTerm, and more. Available on [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=brittanychiang.halcyon-vscode), [Package Control](https://packagecontrol.io/packages/Halcyon%20Theme), [Atom Package Manager](https://atom.io/themes/halcyon-syntax), and [npm](https://www.npmjs.com/package/hyper-halcyon-theme).


Lorem Ipsum is simply dummy text of the printing and typesetting industry.
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
when an unknown printer took a galley of type and scrambled it to make a type s
pecimen book. It has survived not only five centuries, but also the leap into
electronic typesetting, remaining essentially unchanged. It was popularised in
the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
and more recently with desktop publishing software like Aldus PageMaker including
versions of Lorem Ipsum.

## Why do we use it?

It is a long established fact that a reader will be distracted by the readable
content of a page when looking at its layout. The point of using Lorem Ipsum
is that it has a more-or-less normal distribution of letters, as opposed to using
'Content here, content here', making it look like readable English.
Many desktop publishing packages and web page editors now use Lorem
Ipsum as their default model text and a search for 'lorem ipsum' will
uncover many web sites still in their infancy. Various versions have evolved
over the years, sometimes by accident, sometimes on purpose
(injected humor and the like).