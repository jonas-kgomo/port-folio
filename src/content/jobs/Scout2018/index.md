---
date: '2018-04-01'
title: 'Tech Ambassador'
company: 'Iris AI'
location: 'Singularity University'
range: 'Jan 2017- Present'
url: 'https://iris.ai/'
---

- Training the knowledge graph of AI used to collect academic articles, to help researchers find relevant articles
- Ambassador for [IRIS AI](https://iris.ai/),  R&D Science Assistant Building a precise reading list of research documents 
- Helped solidify a brand direction for Project AIUR and search tools that spans both academia and industry

