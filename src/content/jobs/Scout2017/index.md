---
date: '2017-04-01'
title: 'Augmented Reality Developer'
company: 'HoloNext'
location: 'Istanbul'
range: 'Jan - June 2018'
url: 'https://holonext.com/?lang=en'
---

- Developing AI for Photogrammetry and 3D reconstruction 
- Using the <b>reality stack</b>:  ARkit, ARcore and Unity 3D
