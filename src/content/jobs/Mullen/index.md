---
date: '2015-12-21'
title: 'XR Technologist '
company: 'VR First'
location: 'Istanbul'
range: 'Aug 2016 - Dec 2017'
url: 'https://www.upstatement.com/'
---

- Developing VR and AR software for clients in industry level 
- Leading other teams as resident developer at VR First Lab
- Presenting at conferences and serminars about immersive technologies
