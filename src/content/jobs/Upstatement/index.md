---
date: '2018-12-14'
title: 'Quantum Engineer'
company: 'Bohr Technology'
location: 'Poland, Warsaw'
range: 'Dec 2018 - Present'
url: 'https://www.bohr.technology/'
---

- Researching quantum optimization algorithms (Travelling Salesman Problem)
- Work with Python for the Project Q (ETH Zurich) framework.
- Communicate with multi-disciplinary teams of engineers, designers, producers, and clients on a daily basis
- Solving optimization for intractable problems
