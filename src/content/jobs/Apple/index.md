---
date: '2019-06-01'
title: 'Mentor'
company: 'QLatvia'
location: 'Europe'
range: 'April 2018 - Presemt'
url: 'http://qusoft.lu.lv'
---

- QMentor for Quantum programming workshop 

- Three-day long quantum programming workshops in Eastern Europe and the Balkan states from May to August 2019. 

- Presenting Jupyter notebooks using qiskit library 
