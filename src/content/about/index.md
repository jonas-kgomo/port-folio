---
title: 'About Me'
avatar: './me.png' 
photos: './me.png'
skills:
  - QISkit
  - JavaScript
  - LaTex
  - HTML & CSS
  - React
  - C#
  - Java
  - Node.js
  - PyQuil
  - Python
---

I am software engineer based in Istanbul, who enjoys building things that live on the internet.

Shortly after graduating from BSc. Mathematics  at [Istanbul University](http://www.istanbul.edu.tr/en/_), I joined the engineering team at [XR First](https://www.xrfirst.com/) where I got to work on a variety of Mixed Reality projects and tools on a daily basis.

I dedicate my education life to studying quantum information. Currently  I'm searching for Phd in this field. Leisures involve photogrammetry, gaming and coding.
 
Here's a few programming languages I work with:
