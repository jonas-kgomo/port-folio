---
date: '2018-12-23'
title: 'Pin Simple'
path: '/pinsimple'
cover: './pin-simple.png'
github: 'https://github.com/bchiang7/pin-simple'
external: 'https://pin-simple.herokuapp.com/'
tech:
  - Vue
  - Vue Router
  - Express
  - Pinterest API
show: 'true'
---

Pinterest, but just the pictures.

My main motivation for building this was so I could cycle through pins on my boards with the left and right arrow keys and hide the other distracting features that I don't usually use on Pinterest.
